const browserless = require('browserless')({ timeout: 0 })
 
;(async () => {
  console.log('pos1')
  const page = await browserless.page()
  console.log('pos2')
  await browserless.goto(page, {
    url: 'https://poe.ninja/stats',
    abortTypes: ['image', 'media', 'stylesheet', 'font'],
    waitFor: 0
  })
  console.log('pos3')
  await page.waitForSelector('#container > div > table > tbody > tr:nth-child(1) > td.text-right > a');
  console.log('pos4')
  const result = await page.evaluate(() => {
    return document.querySelector('#container > div > table > tbody > tr:nth-child(1) > td.text-right > a').innerHTML;
  })
  console.log(result)

  process.exit()
})()
